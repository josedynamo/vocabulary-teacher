name := "simple-vocabulary-teacher"
organization := "com.jose.reactive"

version := "1.0-SNAPSHOT"

lazy val `simple-vocabulary-teacher` = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.7"

routesGenerator := InjectedRoutesGenerator

com.typesafe.sbt.SbtScalariform.scalariformSettings

routesImport += "binders.PathBinders._"

routesImport += "binders.QueryStringBinders._"

libraryDependencies += filters
// Adds additional packages into Twirl
//TwirlKeys.templateImports += "com.jose.reactive.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "com.jose.reactive.binders._"
