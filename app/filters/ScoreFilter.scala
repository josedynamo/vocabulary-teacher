package filters

import play.api.libs.iteratee.Enumerator
import play.api.mvc.{ Filter, RequestHeader, Result }

import scala.concurrent.Future

class ScoreFilter extends Filter {
  override def apply(nextFilter: RequestHeader => Future[Result])(rh: RequestHeader): Future[Result] = {
    val result = nextFilter(rh)
    import play.api.libs.concurrent.Execution.Implicits._

    result.map {
      response =>
        if (response.header.status == 200 || response.header.status == 406) {
          val correct = response.session(rh).get("correct").getOrElse(0)
          val wrong = response.session(rh).get("wrong").getOrElse(0)
          val score = s"\nYour score is : $correct correct answers and $wrong wrong answers"
          val newBody = response.body andThen Enumerator(score.getBytes("UFT-8"))
          response.copy(body = newBody)
        } else response
    }

  }
}
