import javax.inject.Inject

import play.api.http.HttpFilters
import play.filters.gzip.GzipFilter
import play.filters.headers.SecurityHeadersFilter

class Filters @Inject() (gzipFilter: GzipFilter) extends HttpFilters {
  val filters = Seq(gzipFilter, SecurityHeadersFilter())

}
